FROM python:3.9

RUN mkdir -p /opt/dagster/dagster_home /opt/dagster/app

RUN pip install dagit dagster-postgres

# Dependent packages
RUN pip install pandas

# Copy your code and workspace to /opt/dagster/app
COPY workspace.yaml /opt/dagster/app/
COPY src /opt/dagster/app/src

ENV DAGSTER_HOME=/opt/dagster/dagster_home/

# Copy dagster instance YAML to $DAGSTER_HOME
COPY dagster.yaml /opt/dagster/dagster_home/

WORKDIR /opt/dagster/app

EXPOSE 3000

ENV DAGSTER_PG_USERNAME=postgres
ENV DAGSTER_PG_DB=blockchain
ENV DAGSTER_PG_PASSWORD=mysecretpassword
ENV DAGSTER_PG_HOST=130.193.43.125

ENTRYPOINT ["dagit", "-h", "0.0.0.0", "-p", "3000"]
