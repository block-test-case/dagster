import ssl
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase


def send_mail(data: str, email_to: str):
    # Email routines
    message = MIMEMultipart()
    message['From'] = "testcaseblockchain@gmail.com"
    message['To'] = email_to
    message['Subject'] = 'Report on user-base attachment'
    message.attach(MIMEText("Hello! This is your periodic user-base report", 'plain'))

    # Payload
    payload = MIMEBase('application', 'octate-stream')
    payload.set_payload(data.encode())
    payload.add_header('Content-Disposition', 'attachment; filename=report.txt')
    message.attach(payload)
    text = message.as_string()

    # Sending
    with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=ssl.create_default_context()) as server:
        server.login("testcaseblockchain@gmail.com", "lolkek123")
        server.sendmail("testcaseblockchain@gmail.com", email_to, text)

