from dagster import (
    job,
    schedule,
    repository
)

from src.ops import (
    generate_report,
    send_emails
)


@job
def my_job():
    send_emails(generate_report())


@schedule(cron_schedule="* * * * *", job=my_job, execution_timezone="US/Central")
def my_schedule(_context):
    return {}


@repository
def deploy_docker_repository():
    return [my_job, my_schedule]
