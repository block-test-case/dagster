import smtplib
import pandas as pd

from dagster import (
    op,
    get_dagster_logger
)
from src.resources.db import create_connection
from src.emailing import send_mail


@op
def send_emails(df_report: pd.DataFrame):
    """
    Basic report:
    """
    sql = "SELECT * FROM main.emails"

    with create_connection() as conn:
        df_emails = pd.read_sql(sql, con=conn)

    get_dagster_logger().info(f"Going to send to these emails: {df_emails.__str__()}")

    for email in df_emails.email.values:
        try:
            send_mail(data=df_report.to_string(),
                      email_to=email)
        except smtplib.SMTPRecipientsRefused:
            get_dagster_logger().info(f"Wrong email: {email}")
        except Exception:
            get_dagster_logger().error(f"Smth wrong. Check logs")



