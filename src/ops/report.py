import pandas as pd
from dagster import (
    op
)
from src.resources.db import create_connection


@op
def generate_report() -> pd.DataFrame:
    """
    Basic report:
    """
    sql = """
    SELECT user_id, state, COUNT(*) as n_logins
    FROM users
    GROUP BY user_id, state;
    """

    with create_connection() as conn:
        df_records_aggregated = pd.read_sql(sql, con=conn)
    return df_records_aggregated

