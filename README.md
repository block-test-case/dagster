```
dagster
│ dagster.yml  # File for Single-process Dagster deployment
│ Pipfile | Pipfile.lock not used
| Dockerfile  
└─── src    
    └─── ops  # Pipeline steps logic
        │ report.py  # Generating basic report
        │ send_mail.py  # Sending emails 
    └─── resources  
        | db.py  # Database conection   
    | emailing.py  # Utils for sending emails
    │ repo.py  # Dagster entrypoint file. Pipeline definetion
```